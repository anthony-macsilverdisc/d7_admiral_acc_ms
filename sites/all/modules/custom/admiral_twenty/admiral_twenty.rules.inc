<?php
/**
 * Implementation of hook_rules_action_info()
 */
function admiral_twenty_rules_action_info(){
    return array(
        'admiral_twenty_apply_discount' => array( // This is the name of the callback function
            'label' => t('Apply 20% discount(time-based)'),
            'group' => t('Commerce Line Item'),
            'parameter' => array(
                'line_item' => array(
                    'type' => 'commerce_line_item',
                    'label' => t('Line Item'),
                    'description' => t('The line item on which to apply the volume discount.'),
                    'wrapped' => true, // This is nice, it will be wrapped for us
                    'save' => true,    // And saved too!
                ),
                'discount_percentage' => array(
                    'type' => 'decimal',
                    'label' => t('Discount Percentage'),
                    'description' => t('Specify a discount percentage'),
                ),
                'end_date_day' => array(
                    'type' => 'decimal',
                    'label' => t('Discount End Day'),
                    'description' => t('Specify a day as a number (01 -31) on which the discount expires'),
                ),
                'end_date_month' => array(
                    'type' => 'decimal',
                    'label' => t('Discount End Month'),
                    'description' => t('Specify a month as a number (01-12) on which the discount expires'),
                ),
            ),
        ),
    );
}


/**
 * Rules action that applies a volume discount to an order
 * @param commerce_line_item $line_item
 * @return type
 */
function admiral_twenty_apply_discount($line_item_wrapper, $discount_percentage, $end_date_day, $end_date_month){
    //Get the current year
    $year = date('y');
    $end_date = $year . '/' . $end_date_month . '/' . $end_date_day;
    //Current time
    $current_date = time();
    //Convert the end date into an array
    $end_date_array = explode("/",$end_date);
    $end_date_int= mktime(0,0,0,$end_date_array[1],$end_date_array[2],$end_date_array[0]) ;
    //Set expired to true or false
    $expired = ($current_date > $end_date_int ? true : false);



    if ($expired == false) {
        if($line_item_wrapper->value()->type == 'ajw_product_attributes_line_item_custom'){
            // Find the total discount on the line item
            $discount_quantity = $line_item_wrapper->quantity->value();
            $discount_total_ammount = $discount_quantity * $line_item_wrapper->commerce_unit_price->amount->value() * ($discount_percentage/100);

            // The discount is applied to the unit price, so we need to make sure
            // our discount is per unit
            $discount = $discount_total_ammount / $line_item_wrapper->quantity->value();

            if($discount){
                // Add the discount as a price component
                $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
                    $line_item_wrapper->commerce_unit_price->value(),
                    'twenty_discount',
                    array(
                        'amount' => $discount * -1,
                        'currency_code' => 'GBP',
                        'data' => array()
                    ),
                    0 // NOT included already in the price
                );
            }
        }
    }

}