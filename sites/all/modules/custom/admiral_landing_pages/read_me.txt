Admiral Landing pages

Uses hook_init to intercept page loads. It determines which page is being loaded from
a list of page names in a variable, admiral_landing_pages. The variable is generated
from a taxonomy vocabuluary, LandingPages. Pages are created from a page callback and
content is loaded from LandingPages and Slider Landing Pages.

Management is provided from admin/config in the modules own section. When pages are
created, a call to menu_rebuild() rebuilds all menu items and the landing page menu items
 are generated dynamically from the variable, admiral_landing_pages. Pages can be deleted
 and modified