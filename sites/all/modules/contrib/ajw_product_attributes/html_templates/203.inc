<body>
<div id="left-panel" style="padding-top:212px;max-height: 1737px; height: 1737px; width: 50%; float: left;">
    <div id="left-panel-image">203
        <? if (isset($horizontal_left_panel_image) && $horizontal_left_panel_image != '') { ?>
        <div style="width:1472px; height:826px; margin-left:auto;margin-right:auto;text-align:center"><?php echo $horizontal_left_panel_image ?></div>
        <? } ?>
        </div>
</div>
<div id="right-panel" style="padding-top:110px;max-height:1737px;height:1737px;width:50%;float:left;">
    <div id="greeting" style="height:874px;width: 2240px; margin-bottom:50px; margin-left: auto; margin-right: auto; background:white;">
       <?php echo $greeting ?>
    </div>
    <div id="signature" style="height:293px; width: 1201px; margin-left: auto; margin-right: auto; background:white;text-align:center">
        <? if ($horizontal_signature_image != '') { ?>
        <div><?php echo $horizontal_signature_image ?></div>
        <? } ?>
    </div>
    <div id="bottom-wrapper" style="width:100%;max-height:483px;margin-left:auto;margin-right:auto;background:white">
        <div id="address" style="height:374px;width:312px;margin-left:auto;margin-right:auto;">
            <?php echo $address ?>
        </div>                 
    </div>
</div>
</body>

