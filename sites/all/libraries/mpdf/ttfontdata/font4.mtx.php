<?php
$name='Redressed';
$type='TTF';
$desc=array (
  'Ascent' => 931,
  'Descent' => -241,
  'CapHeight' => 730,
  'Flags' => 4,
  'FontBBox' => '[-146 -241 1046 1149]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 211,
);
$up=-123;
$ut=20;
$ttffile='/usr/local/apache/virtualhosts/drupal7_generic/drupal7_accdev/sites/all/libraries/mpdf/ttfonts/Redressed.ttf';
$TTCfontID='0';
$originalsize=79220;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='font4';
$panose=' 0 0 2 0 5 6 0 0 0 2 0 3';
$haskerninfo=false;
$unAGlyphs=false;
?>