<body>
<div id="left-panel" style="padding-top:390px;height:2474px;width:50%;float:left;">
    <div id="left-panel-image">161
            <div style="width:1204px;max-height:1285px;overflow:hidden;min-height:1285px;height:1285px;margin-left:auto;margin-right:auto;text-align:center">
                <?php echo $left_panel_image ?>
            </div>
    </div>
</div>
<div id="right-panel" style="padding-top:628px;height:2474px;width:50%;float:left;">
    <div id="greeting" style="height:774px;width:1506px;margin-left:auto;margin-right:auto;background:white;">
                   <?php echo $greeting ?>
    </div>

    <div id="address" style="height:374px;width:1506px;margin-left:auto;margin-right:auto;margin-top:62px;background:white;">
                    <?php echo $address ?>
    </div>

    <div id="bottom-wrapper" style="width:1741px;margin-left:auto;margin-right:auto;vertical-align:bottom;display:table-cell;text-align:center;">
        <table style="width:1740px;">
        <tr>
            <td style="padding-left:81px;" width="586" height="561" valign="bottom">
                    <? if ($centre_bottom != '') { ?>
                        <div id="left-image" style="max-height:295px; max-width:591px;">
                            <?php echo $centre_bottom ?>
                        </div>
                    <? } ?>
            </td>
            <td align="right" style="max-height: 295px; max-width: 586px; padding-right:81px;" width="586" height="295" valign="bottom">
                    <? if ($right_bottom != '') { ?>
                        <div id="right-image" style="height:295px;max-height:295px; margin-right:81px;width:591px; max-width:591px;">
                            <?php echo $right_bottom ?>
                        </div>
                    <? } ?>
            </td>
        </tr>
        </table>
    </div>
</div>
</body>