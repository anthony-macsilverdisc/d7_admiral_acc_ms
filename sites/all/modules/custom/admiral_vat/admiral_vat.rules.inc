<?php
/**
 * Implementation of hook_rules_action_info()
 */
function admiral_vat_rules_action_info(){
    return array(
        'admiral_vat_apply_vat' => array( // This is the name of the callback function
            'label' => t('Apply 20% VAT'),
            'group' => t('Commerce Line Item'),
            'parameter' => array(
                'line_item' => array(
                    'type' => 'commerce_line_item',
                    'label' => t('Line Item'),
                    'description' => t('The line item on which to apply the volume discount.'),
                    'wrapped' => true, // This is nice, it will be wrapped for us
                    'save' => true,    // And saved too!
                ),
                'vat_rate' => array(
                    'type' => 'decimal',
                    'label' => t('VAT Rate'),
                    'description' => t('Specify vat rate as a number. For example enter 20 for 20%'),
                ),
            ),
        ),
    );
}


/**
 * Rules action that applies a volume discount to an order
 * @param commerce_line_item $line_item
 * @return type
 */
function admiral_vat_apply_vat($line_item_wrapper, $vat_rate){
    if($line_item_wrapper->value()->type == 'ajw_product_attributes_line_item_custom'){
        //Get the current calculated price of the product item
        $price = $line_item_wrapper->commerce_unit_price->value();
        $calculated_price = commerce_price_component_total($price);

        $vat_amount = $calculated_price['amount'] * ($vat_rate/100);

        if($vat_amount){
            // Add the discount as a price component
            $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
                $line_item_wrapper->commerce_unit_price->value(),
                'admiral_vat',
                array(
                    'amount' => $vat_amount,
                    'currency_code' => 'GBP',
                    'data' => array()
                ),
                0 // NOT included already in the price
            );
        }
    }
}