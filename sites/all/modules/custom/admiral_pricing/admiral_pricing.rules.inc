<?php
//Rules implementation of custom pricing for the Admiral site
//First off we add some actions. The action used on the ACC site
//is admiral_quantity discount.
//admiral_pricing_apply_volume_discount demonstrates an alternative approach
//which adds a price component which is calculated within the cart totals
//It provides an extra line
/**
 * Implementation of hook_rules_action_info()
 */
function admiral_pricing_rules_action_info(){

  $actions = array(
    'admiral_quantity_discount' => array(
      'label' => t('Removes x amount based on Admiral pricing'),
      'admiral breakpoint' => t('Specify a breakpoint for example 50 cards'),
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line item')
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Amount'),
          'description' => t('Specify a breakpoint for example 50 cards'),
        ),
          'discount_price' => array(
              'type' => 'decimal',
              'label' => t('Discount price'),
              'description' => t('Specify the discounted price'),
          ),
        'component_name' => array(
          'type' => 'text',
          'label' => t('Price component type'),
          'description' => t('Price components track changes to prices made during the price calculation process, and they are carried over from the unit price to the total price of a line item. When an order total is calculated, it combines all the components of every line item on the order. When the unit price is altered by this action, the selected type of price component will be added to its data array and reflected in the order total display when it is formatted with components showing. Defaults to base price, which displays as the order Subtotal.'),
          'options list' => 'commerce_line_item_price_component_options_list',
          'default value' => 'base_price',
        ),
        'round_mode' => array(
          'type' => 'integer',
          'label' => t('Price rounding mode'),
          'description' => t('Round the resulting price amount after performing this operation.'),
          'options list' => 'commerce_round_mode_options_list',
          'default value' => COMMERCE_ROUND_HALF_UP,
        ),
      ),
      'group' => t('Commerce Line Item'),
    ),
  );
  return $actions;
}




//Admiral require that the first x amount are charged at 1.99 and further cards are charged at
//0.99. This function (action) takes care of the calculation working directly on the prices
/**
 * Rules action: add an amount to the unit price.
 */
function admiral_quantity_discount($line_item, $amount, $discount_price,  $component_name, $round_mode) {
  if (is_numeric($amount)) {
    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    //Get the current quantity
    $quantity = $line_item->quantity;
    //Get the unit price
    $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price', TRUE);

    if($quantity > $amount) {

      //Get the quantities to be charged at lower and upper prices
      $quantity_at_full = $amount;
      $quantity_at_discount = $quantity - $amount;

      $first_total = $quantity_at_full * $unit_price['amount'];
      $second_total = $quantity_at_discount * ($discount_price * 100);

      $grand_total = $first_total + $second_total;

      //Calculate new unit price
      $new_unit_price = $grand_total / $quantity;

      // Calculate the updated amount and create a price array representing the
      // difference between it and the current amount.
      $current_amount = $unit_price['amount'];

      $updated_amount = commerce_round($round_mode, $new_unit_price - $current_amount);

      $difference = array(
        'amount' => $updated_amount,
        'currency_code' => $unit_price['currency_code'],
        'data' => array(),
      );
      // Set the amount of the unit price and add the difference as a component.
      $wrapper->commerce_unit_price->amount = $current_amount + $updated_amount;

      $wrapper->commerce_unit_price->data = commerce_price_component_add(
        $wrapper->commerce_unit_price->value(),
        $component_name,
        $difference,
        TRUE
      );
    }
  }
}