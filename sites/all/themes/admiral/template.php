<?php


// Swop image style for Horizontal cards (left_panel_image)
function admiral_image_style($variables) {

     
if(!empty($_REQUEST['product_id']) && $variables['style_name'] == 'left_panel_image_vertical') {
    $product_id = $_REQUEST['product_id'];
    $product = commerce_product_load($product_id);
    // $x = arg(1);
    if(isset($product) && $product->type == 'admiral_card_horizontal') {
        
        $variables['width'] = 300;   
        $variables['height'] = 315;

        $variables['attributes'] = array(
            'class' => $variables['style_name'],
        );
        $variables['style_name'] = 'left_panel_image';
        $variables['path'] = image_style_url($variables['style_name'], $variables['path']);
        return theme('image', $variables);
        
    } elseif(isset($product) && $product->type == 'admiral_card_vertical') {

        $variables['width'] = 386;   
        $variables['height'] = 412;

        $variables['attributes'] = array(
            'class' => $variables['style_name'],
        );
        $variables['style_name'] = 'left_panel_image_vertical';
        $variables['path'] = image_style_url($variables['style_name'], $variables['path']);
        return theme('image', $variables);
    }
}
    $dimensions = array(
        'width' => $variables['width'],
        'height' => $variables['height'],
    );

    image_style_transform_dimensions($variables['style_name'], $dimensions);

    $variables['width'] = $dimensions['width'];
    $variables['height'] = $dimensions['height'];

    // Determine the URL for the styled image.
    $variables['path'] = image_style_url($variables['style_name'], $variables['path']);
    return theme('image', $variables);  
}




/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * admiral theme.
 */

function admiral_form_alter(&$form, &$form_state, $form_id)
{


    if ($form_id == 'views_form_commerce_cart_form_default') {

        $path = drupal_get_path('module', 'ajw_product_attributes');
        drupal_add_js($path . '/js/jquery.jstepper.min.js');

        $edit_fields = $form['edit_quantity'];
        $class = 'integer';
        foreach($edit_fields as $key => $edit_field) {

            if (is_numeric($key)) {
                $form['edit_quantity'][$key]['#attributes']['class'][] = $class;
            }

            //dsm($edit_field);
        }
    }

    if (strpos($form_id, 'product_attributes') != false) {

        if(isset($form['left-panel']['fields']['admiral_left_panel_image'])) {

            $lang = $form['left-panel']['fields']['admiral_left_panel_image']['#language'];
            $form['left-panel']['fields']['admiral_left_panel_image'][$lang][0]['#description'] =
                t('Images must be a minimum of <strong>300dpi</strong><br/>and a minimum of <strong>1748x2480px</strong>.');
        }
        if (isset($form['right-panel'])) {
            $form['right-panel']['fields']['admiral_client_address']['und'][0]['#format'] = 'add_to_cart_2';
            $form['right-panel']['fields']['field_greeting']['und'][0]['#format'] = 'add_to_cart';
            $fields = array('admiral_right_panel_centre_image', 'admiral_right_panel_signature', 'admiral_right_panel_centre_image',
                'admiral_right_panel_right_image', 'admiral_right_panel_main', 'admiral_right_panel_left_image');
            foreach($fields as $field) {
                $lang = $form['right-panel']['fields'][$field]['#language'];
                if ($form['right-panel']['fields'][$field][$lang][0]['#default_value']['fid'] == 0) {
                    $form['right-panel']['fields'][$field][$lang][0]['#description'] =
                        t('Images must be a minimum of <strong>300dpi</strong>.');
                }
                else {
                    $form['right-panel']['fields'][$field][$lang][0]['#description'] = '';
                }
            }
        }
    }

    //Mods to search api forms
    if ($form_id == 'views_exposed_form') {
        $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/small-search-icon.png');
        $form['search_api_views_fulltext']['#size'] = 36;
    }

    //Webform mods
    if (isset($form['submitted'])) {
        foreach ($form['submitted'] as $key => $value) {
            // dpm($value['#type']);
            if ('textfield' == $value['#type']) {

                //You company is not a required field so don't add the *
                if ($form['submitted'][$key] != 'company_name') {
                    $form['submitted'][$key]['#attributes']['placeholder'] = t('Your ' . $value['#title'] . ' *');
                }

                if ($key == 'company_name' || $key == 'your_company') {
                    $form['submitted'][$key]['#attributes']['placeholder'] = t('Your ' . $value['#title']);
                }
                if ($key == 'phone_number' || $key == 'your_phone_number') {
                    // dpm($form['submitted'][$key]);
                    $form['submitted'][$key]['#attributes']['placeholder'] = t('Your ' . $value['#title'] . ' *');
                    $form['submitted'][$key]['#element_validate'] = array('element_validate_number');
                }

                // $form['submitted'][$key]['#title'] = '';
                $form['submitted'][$key]['#title_display'] = 'invisible';
            }

            // dpm($value['#type']);

            if ($value['#type'] == 'webform_email') {
                $form['submitted'][$key]['#attributes']['placeholder'] = t('Your ' . $value['#title'] . ' *');
                $form['submitted'][$key]['#title_display'] = 'invisible';
                // $form['submitted'][$key]['#title'] = '';
            }

            if ($value['#type'] == 'textarea') {
                $form['submitted'][$key]['#attributes']['placeholder'] = t('Your ' . $value['#title'] . ' *');
                $form['submitted'][$key]['#title_display'] = 'invisible';
                // $form['submitted'][$key]['#title'] = '';

            }
        }
    }

    //Restructure the user form
    if ($form_id == 'user_login') {
        //Add new title
        $form['new_title'] = array(
            '#markup' => '<div class="user-login-title"><h2>' . t('Existing Customer Login') . '</h2></div>',
            '#weight' => -1000,
        );

        $form['name'] = array(
            '#type' => 'textfield',
            '#title' => 'Username',
            '#size' => 60,
            '#maxlength' => 60,
            '#required' => true,
            '#description' => '',
        );

        $form['pass'] = array(
            '#type' => 'password',
            '#title' => 'Password',
            '#description' => '',
            '#required' => true,
        );


        $password_link = l(t('Forgot Your Password?'), 'user/password', array('attributes' => array('class' => 'reset-link')));

        $form['some_text'] = array(
            '#markup' => $password_link,
            '#weight' => 100,
        );
        $form['actions']['#suffix'] = '</div><div class="create-account clearfix">
        <h2>New to Admiral Charity Cards?</h2><a href="/user/register" class="login-register button button-primary" title="Create a new account">Sign Up</a></div>';
    }

    if ($form_id == 'user_register_form') {
        $form['actions']['submit']['#value'] = 'Create New Account';
    }


    //Bespoke card form
    if ($form_id == 'webform_client_form_128') {

        if (isset($form['submitted'])) {
            foreach ($form['submitted'] as $key => $value) {
                //dsm($form_id);
                if ($form['submitted'][$key]['#type'] == 'fieldset') {

                    foreach ($form['submitted'][$key] as $field_key => $field_value) {
                        //dsm($form_id);
                        if (isset($form['submitted'][$key][$field_key]['#type'])) {
                            switch ($form['submitted'][$key][$field_key]['#type']) {
                                case  'select' :
                                    //Do something
                                    break;
                                case 'textarea' :
                                    $form['submitted'][$key][$field_key]['#attributes']['placeholder'] = t($field_value['#title']) . '...';
                                    break;
                                case 'textfield' :
                                    $form['submitted'][$key][$field_key]['#attributes']['placeholder'] = t($field_value['#title']) . ' *';
                                    break;
                                case 'webform_email' :
                                    $form['submitted'][$key][$field_key]['#attributes']['placeholder'] = t($field_value['#title']) . ' *';
                            }
                        }
                        //We have a special case on this form as a long line of text is required
                        if ($field_key == 'additional_info') {
                            $form['submitted'][$key][$field_key]['#attributes']['placeholder'] = t('Type your additional details, address etc.') . '...';
                        }
                        //We have a special case as this field needs alternative text
                        if ($field_key == 'how_many_cards') {
                            $form['submitted'][$key][$field_key]['#attributes']['placeholder'] = t('25 Minimum');
                        }
                    }
                }
            }
        }
    }


    //Add to cart form modifications specific to Admiral add to cart form

    if (strstr($form_id, 'ajw_product_attributes_add_to_cart_')) {

        //Rearrange fields if we are on Step 1
        if ($form_state['step'] == 1) {

            //Alter the next button to show Start personalising
            //$form['buttons']['forward']['#value'] = 'Start Personalising';
            $form['buttons']['forward']['#attributes'] = array('class' => array('first-step'));


            //Change the text displayed on this field
            $form['line_item_fields']['admiral_charity_select']['und']['#options']['_none'] = 'Choose a charity';
        }

    }

    //Remove Teammember box from user forms
    if ($form_id == 'user_profile_form') {
        $roles = $GLOBALS['user']->roles;

        if (!in_array('team member', $roles)) {
            $form['field_team_member_info']['#access'] = false;
            $form['picture']['#access'] = false;
        }
    }
}


function admiral_page_alter(&$page)
{

    //Add the main menu to the footer in the first position
    if (isset($page['navigation']['system_main-menu'])) {
        $main_menu_block['system_main_menu'] = $page['navigation']['system_main-menu'];
        $page['footer_1'] = $main_menu_block;
    }
    //Add the social connections menu in the last position
    /*if (isset($page['footer_3']['menu_menu-social-connection'])) {
      $social_menu_block = $page['footer_3']['menu_menu-social-connection'];
      $page['cart']['menu_menu-social-connection'] = $social_menu_block;
    }*/
    //add Social icons to sidebar_second on contact us page
    if (isset($page['content']['system_main']['nodes']) && in_array('105', $page['content']['system_main']['nodes'])) {

        //Move contact us body to content_first region
        if (isset($page['content']['system_main']['nodes']['105']['body'])) {
            $contact_body = $page['content']['system_main']['nodes']['105']['body'];
            $page['content_first']['body'] = $contact_body;
            $page['content_first']['#region'] = 'content_first';
            $page['content_first']['#theme_wrappers'] = array('0' => 'region');
            $page['content_first']['#sorted'] = true;
            unset($page['content']['system_main']['nodes']['105']['body']);
        }
    }

    //Add the contact form to the Choose your charity page
    if (isset($page['content']['system_main']['nodes']) && isset($page['content']['system_main']['nodes']['136'])) {
        $contact_form = $page['sidebar_second']['webform_client-block-105'];
        $page['blogrelated']['webform_client-block-105'] = $contact_form;
        unset($page['sidebar_second']);
    }

}

/**
 * @param array $variables
 * @return string
 * Adds a delimiter between menu items
 */
function admiral_menu_link(array $variables)
{
    $delimiter = '';
    // Only add delimiter to the main menu
    if (strpos($variables['element']['#theme'], 'main_menu') !== FALSE) {
        // Only add delimiter if item is not the last one
        if (!in_array('last', $variables['element']['#attributes']['class'])) {
            $delimiter = '<li class="delimiter">|</li>';
        }
    }
    $element = $variables['element'];
    $sub_menu = '';
    if ($element['#below']) {
        $sub_menu = drupal_render($element['#below']);
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    // Add delimiter just before the closing </li> tag
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . $delimiter . "</li>\n";
}











/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */


function admiral_breadcrumb($variables) {

/* print "arg 0 : " . arg(0) . "<br />";
 print "arg 1 : " . arg(1) . "<br />";
 print "arg 2 : " . arg(2) . "<br />";
 print "arg 3 : " . arg(3) . "<br />";

 print "type: " . $type . "<br />";
 print "taxonomy: " . $taxType . "<br />";
 print "term : " . $termName . " : " . $tid;
 print "<br />";
 print "vocab :" . $vocabName . " : " . $vid;
 print "<br />";
 print "nodeType : " .$nodeType;*/


    $breadcrumb = $variables['breadcrumb'];

    // MB: Check the type of page and the following code will run to set the breadcrumb
    $type = arg(0);
    $taxType = arg(1);

    // dsm($type);

    if($type == 'node'){

        //Get the node type by loading the node id into the node object and accessing it as follows (nid is loacted at arg(1) for a node)
        $node = node_load(arg(1));
        $nodeType = $node->type;

        //dsm($nodeType);

        if($nodeType == "blog_post"){
            $breadcrumb[1] = '<a href="/blog">' . 'Blog' . '</a>';
            $breadcrumb[2] = $node->title;
        }


        else if($nodeType == "contact_page"){
            $breadcrumb[1] = $node->title;
        }

        else if($nodeType == "service"){
            $breadcrumb[1] = '<a href="/our-services">' . 'Services' . '</a>';
            $breadcrumb[2] = $node->title;
        }

        else if($nodeType == "page"){
            $breadcrumb[1] = $node->title;
        }

        else if($nodeType == "our_work"){
            $breadcrumb[1] = '<a href="/our-work">' . 'Our Work' . '</a>';
            $breadcrumb[2] = $node->title;
        }

        else if($nodeType == "webform"){
            $breadcrumb[1] = $node->title;
        }

                else if($nodeType == "admiral_card_vertical"){
                    // dsm($node);
                    // $term = taxonomy_term_load($node->field_product_category[LANGUAGE_NONE][0]['tid']);
                    // dsm($term);

                    // $path_alias = drupal_get_path_alias(taxonomy_term_uri($term) );
                    // $path_alias = drupal_lookup_path('alias', "taxonomy/term/".$node->field_product_category[LANGUAGE_NONE][0]['tid']);
                    // $breadcrumb[1] = '<a href="'.$path_alias.'">' . $term->name . '</a>';
                    $breadcrumb[1] = $node->title;
        }

                else if($nodeType == "admiral_card_horizontal"){
                    // $breadcrumb[1] = '<a href="/card-collections">' . 'Our Work' . '</a>';
            $breadcrumb[1] = $node->title;
        }


    }


    if($type == 'blog'){
        $breadcrumb[1] = "Blog";
    }


    if($type == 'our-work'){
        $breadcrumb[1] = "Our Work";
    }

    if($type == 'testimonials'){
        $breadcrumb[1] = "Testimonials";
    }

    if($type == 'our-services'){
        $breadcrumb[1] = "Our Services";
    }
    if($type == 'card-collections'){
        $breadcrumb[1] = "Card collections";
    }


    if($taxType == 'category'){
        $blogcategory = arg(2);
        $breadcrumb[1] = '<a href="/blog">' . 'Blog' . '</a>';
        $breadcrumb[3] = ucfirst($blogcategory);
    }


    if (!empty($breadcrumb)) {
        // Provide a navigational heading to give context for breadcrumb links to
        // screen-reader users. Make the heading invisible with .element-invisible.
        $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

        $output .= '<div class="breadcrumb">' . implode(' > ', $breadcrumb) . '</div>';
        return $output;
    }

}

/**
 * Themes a price components table.
 *
 * @param $variables
 *   Includes the 'components' array and original 'price' array.
 */
function admiral_commerce_price_formatted_components($variables) {
    // Add the CSS styling to the table.
    drupal_add_css(drupal_get_path('module', 'commerce_price') . '/theme/commerce_price.theme.css');

    // Build table rows out of the components.
    $rows = array();

    foreach ($variables['components'] as $name => $component) {
        //Create an extra row showing new subtotal after discount

        if ($name == 'admiral_vat') {
            //@todo Don't show if there is no discount
            //Calculate the subtotal
            $net_price = $variables['components']['base_price']['price']['amount'];
            //Twenty Percent Discount
            if (isset($variables['components']['ten_discount']) || isset($variables['components']['twenty_discount']) || isset($variables['components']['admiral_discount'])) {
                //$discount = $variables['components']['twenty_discount']['price']['amount'];
                //$discounted_subtotal = $net_price + $discount;

                //Start
                if (isset($variables['components']['ten_discount'])) {
                    $discount = $variables['components']['ten_discount']['price']['amount'];
                }

                if (isset($variables['components']['twenty_discount'])) {
                    $discount = $variables['components']['twenty_discount']['price']['amount'];
                }

                $discounted_subtotal = $net_price + $discount;

                $currency_code = $variables['components']['base_price']['price']['currency_code'];
                $formatted_subtotal = commerce_currency_format($discounted_subtotal, $currency_code);

                $rows[] = array(
                    'data' => array(
                        array(
                            'data' => 'Subtotal',
                            'class' => array('discounted-subtotal'),
                        ),
                        array(
                            'data' => $formatted_subtotal,
                            'class' => array('component-total'),
                        ),
                    ),
                    'class' => array(drupal_html_class('component-type-' . $name)),
                );
            }
        }

        $rows[] = array(
            'data' => array(
                array(
                    'data' => $component['title'],
                    'class' => array('component-title'),
                ),
                array(
                    'data' => $component['formatted_price'],
                    'class' => array('component-total'),
                ),
            ),
            'class' => array(drupal_html_class('component-type-' . $name)),
        );

    }

    return theme('table', array('rows' => $rows, 'attributes' => array('class' => array('commerce-price-formatted-components'))));
}

/**
 * Implements hook_views_pre_render().
 */
function admiral_views_pre_render(&$view) {
  if ($view->name == 'slider') {
    $results = &$view->result;
    foreach ($results as $key => $result) {

      $html = $result->field_field_slider_image[0]['rendered'];
      $link = (isset($result->_field_data['nid']['entity']->field_content_link['und'][0]['safe_value']) ? $result->_field_data['nid']['entity']->field_content_link['und'][0]['safe_value'] : FALSE);
      if ($link !== FALSE) {
        $link_path = '<a href="/' . $link . '">' . $html . '</a>';
        $result->field_field_slider_image[0]['rendered'] = $link_path;
      }
    }
  }
}