<?php
/**
 * @file admiral_samples.module
 */

/**
 * @return array
 * Implements hook_commerce_price_component_type_info()
 */
function admiral_samples_commerce_price_component_type_info() {
  return array(

    'sample' => array(
      'title' => t('Free Sample'),
      'weight' => 60,
    ),
  );
}

/**
 * Implements hook_menu()
 */
function admiral_samples_menu() {
  $items['request-a-sample'] = array(
    'title' => t('Request a Sample'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('order_sample_cards'),
    'access callback' => TRUE,
    'expanded' => TRUE,
    'menu_name' => 'secondary-navigation',
    'type' => MENU_NORMAL_ITEM,
    'weight' => -100,
  );

  return $items;

}


function order_sample_cards($form, &$form_state) {

  //This form provides the means for a user to select six cards as samples
  //from the full range of cards available

  //Load all the nodes that are card related
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', array('admiral_card_horizontal', 'admiral_card_vertical'))
    ->propertyCondition('status', 1)
    //->fieldCondition('field_card_image', 'fid', 'NULL', '!=')
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
  $result = $query->execute();

  if (isset($result['node'])) {
    $product_nids = array_keys($result['node']);
    $products = entity_load('node', $product_nids);
  }

  $form['introduction'] = array(
    '#type' => 'item',
    '#markup' => '<div class="samples_intro">Requesting a sample couldn&#44;t be easier, simply select your card (up to six) from the
    list below and click the "Order Samples" button. Sample cards are free of charge and delivered within five working days.
    Please note that due to high postage costs we are unable to send samples outside of the UK.</div>'
  );

  //Create container for form elements
  if ($products) {
    $form['cards_region'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'card-region'
        ),
      ),
      '#weight' => 10,
      '#tree' => TRUE,
    );
  }

  foreach ($products as $delta => $product) {

    //entity_metadata_wrapper('node', $delta);
    $wrapper = entity_metadata_wrapper('node', $delta);
    $title = $wrapper->title_field->value();
    $image_file = $wrapper->field_card_image->value();
    $image_html = theme('image_style', array(
      'style_name' => 'thumbnail',
      'path' => $image_file['uri']
    ));


    $form['cards_region'][$delta] = array(
      '#type' => 'checkbox',
      '#prefix' => '<div class="checkbox-wrapper"><div class="image-thumb">' . $image_html . '</div>',
      '#title' => t($title),
      '#suffix' => '</div>',
    );
  }


  $form['buttons'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'buttons'
      ),
    ),
    '#weight' => 100,
    '#tree' => TRUE,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Order Samples',
  );

  return $form;
}

/**
 * validate handler
 * @param $form
 * @param $form_state
 *
 */
function order_sample_cards_validate($form, &$form_state) {
  //loop through $form['cards_region']['products']
  //Customer can only order 6

  $values = $form_state['values']['cards_region'];

  $card_count = '';

  foreach ($values as $key => $value) {

    if (is_numeric($key) && $value == TRUE) {
      $card_count++;
    }
  }

  //Set an error if the client has selected more than six cards
  if ($card_count > 6) {
    form_set_error('cards', t('You can only select six cards as samples'));
  }
  if ($card_count < 1) {
    form_set_error('cards', t('Please select a card. You may choose up to six cards as samples'));
  }
}

/**
 * submit handler
 * @param $form
 * @param $form_state
 *
 */
function order_sample_cards_submit($form, &$form_state) {

  global $user;

  //Collect the product values
  $values = $form_state['values']['cards_region'];

  $selected_cards = array();


  foreach ($values as $key => $value) {
    if (is_numeric($key) && $value == TRUE) {
      array_push($selected_cards, $key);
    }
  }

  //Load all the product displays to find product id

  $chosen_products = array();

  foreach ($selected_cards as $selected_card) {

    $wrapper = entity_metadata_wrapper('node', $selected_card);

    $product = $wrapper->field_product->value();

    //Check how many products are associated with this display if there are more than one
    //it cannot be a card.

    if (count($product) < 1) {
      form_set_error('none_card', t('This product cannot be added to cart'));

      return;
    }

    $chosen_products[] = $product[0];
  }

  foreach ($chosen_products as $delta => $product) {

    $product_id = $product->product_id;

    if ($product = commerce_product_load($product_id)) {
      // Create a new product line item for it.
      $line_item = commerce_product_line_item_new($product, 1);

      //Adjust the price to zero for samples
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      //Get the unit price
      $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);

      rules_invoke_event('commerce_product_calculate_sell_price', $line_item);

      // Default to the current user if a uid was not passed in.
      $uid = $user->uid;
      $combine = FALSE;
      $result = commerce_cart_product_add($uid, $line_item, $combine);
    }
  }
}


/**
 * this function adds the cards selected to the cart
 * this is a special case as there is no charge.
 * @param $selected_cards
 */
function add_sample_cards_order($selected_cards) {


}


/**
 * implements hook_form_alter
 */
function admiral_samples_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'views_form_commerce_cart_form_default') {
    array_unshift($form['#validate'], 'reset_minimum_quantities_validate');

    if (isset($form['edit_quantity'])) {
      $edit_values = $form['edit_quantity'];
      foreach($edit_values as $key=>$edit_value) {

        if (is_array($edit_value) && $edit_value['#default_value'] == 1) {
          $form['edit_quantity'][$key]['#disabled']= TRUE;
          $form['edit_quantity'][$key]['#attributes'] = array('class' => array('disabled-edit'));
        }
      }
    }
  }
}


/**
 * @param $form
 * @param $form_state
 * Posts a warning if the client reduces the value below 25
 */
function reset_minimum_quantities_validate(&$form, &$form_state) {
  if (isset($form_state['values']['edit_quantity'])) {
    $edit_values = $form_state['values']['edit_quantity'];
    foreach($edit_values as $key => $edit_value) {
      if ($edit_value > 1 && $edit_value < 25) {
        form_set_error('too_little', 'You must order a minimum of 25 cards');
      }
    }
  }
}